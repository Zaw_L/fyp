using LinearAlgebra
include("../lib/generateBetaNoise.jl")

const ħ = 0.658211951E-3 # ħ ÷ 1meV, units of 1/ω
const ∠HZH = [
       2    nothing nothing;
    nothing  -0.5   nothing;
    nothing   0.5   nothing;
    nothing    1    nothing;
     -0.5    -0.5     0.5  ;
      0.5    -0.5    -0.5  ;
      0.5     -1     -0.5  ;
     -0.5   nothing nothing;
      0.5   nothing nothing;
       1    nothing nothing;
      0.5     0.5     0.5  ;
     -0.5     0.5    -0.5  ;
      0.75    -1     -0.75 ;
     -0.75    -1      0.75 ;
       1     -0.5   nothing;
    nothing  -0.5      1   ;
    nothing   0.5     0.5  ;
      0.5     0.5      1   ;
      0.5    -0.5      1   ;
    nothing  -0.5     0.5  ;
      0.5     0.5   nothing;
       1      0.5     0.5  ;
      0.5    -0.5   nothing;
       1     -0.5     0.5
]
const cτ = denominator(rationalize(sqrt(2/3),tol=1E-4))
const Nₛ = ceil(Int,10^4/cτ) * cτ

discreteτ(τ;p...)   = Int.(round.(τ / ħ / π / abs(p[:U]-p[:V]) * p[:t]^2 * 32 * cτ))
discreteτ⁻¹(τ;p...) = τ/cτ/32/p[:t]^2*abs(p[:U]-p[:V])*π*ħ

function tqdpulseseq(S::AbstractArray{<:Int},τₖ=0;p...)
    tₗ = zeros(1)
    tᵣ = zeros(1)
    τ  = Float64[τₖ]
    τI = [1]
    for i in S
        tₗₖ,tᵣₖ,τₖ = tqdpulseC(i,τ[end];p...)
        append!(tₗ,tₗₖ)
        append!(tᵣ,tᵣₖ)
        append!(τ,τₖ)
        push!(τI,length(τ))
    end
    return tₗ,tᵣ,τ,τI
end

function tqdnoiseypulseseq(S::AbstractArray{<:Int},τ₀=0;p...)
    keysP = keys(p)
    tₗₖ,tᵣₖ,τₖ,τI = tqdpulseseq(S,τ₀;p...)
    dτ = diff(τₖ)
    ndτ = discreteτ(dτ;p...)
    nτ = cumsum(ndτ)
    pushfirst!(nτ,0)
    dB = :dB in keysP ? p[:dB] : 30
    N = nτ[end]

    Nfactor = N < βN(dB) ? ceil(Int,βN(dB)/N) : 1

    N *= Nfactor
    ndτ *= Nfactor
    nτ *= Nfactor
    dτ = discreteτ⁻¹(ndτ;p...)/Nfactor
    τₖ = τₖ[1] .+ discreteτ⁻¹(nτ;p...)/Nfactor

    Δτ = (τₖ[end]-τₖ[1])/N

    β = :β in keysP ? p[:β] : 1

    tₗ = Float64[tₗₖ[1]]
    tᵣ = Float64[tᵣₖ[1]]
    ε  = Float64[p[:ε]]
    εₘ = Float64[p[:εₘ]]
    τ  = τₖ[1] .+ discreteτ⁻¹(0:N;p...)/Nfactor

    δt  = :δt  in keysP ? p[:δt]  : p[:t]
    δtₗ = :δtₗ in keysP ? p[:δtₗ] : δt
    δtᵣ = :δtᵣ in keysP ? p[:δtᵣ] : δt
    δε₀ = :δε  in keysP ? p[:δε]  : ( :δεₘ in keysP ? p[:δεₘ] : false )
    δε  = :δε  in keysP ? p[:δε]  : ( δε₀ == false ? (p[:ε]  ≈ 0 ? p[:εₘ] : p[:ε]) : δε₀ )
    δεₘ = :δεₘ in keysP ? p[:δεₘ] : ( δε₀ == false ? (p[:εₘ] ≈ 0 ? p[:ε]  : p[:εₘ]) : δε₀ )
    
    δt  *= βnoise(τₖ[end]-τₖ[1],dB,N=N,β=β,Nₛ=Nₛ)
    δtₗ *= βnoise(τₖ[end]-τₖ[1],dB,N=N,β=β,Nₛ=Nₛ)
    δtᵣ *= βnoise(τₖ[end]-τₖ[1],dB,N=N,β=β,Nₛ=Nₛ)
    δε  *= βnoise(τₖ[end]-τₖ[1],dB,N=N,β=β,Nₛ=Nₛ)
    δεₘ *= βnoise(τₖ[end]-τₖ[1],dB,N=N,β=β,Nₛ=Nₛ)

    for k in 2:length(nτ)
        arrI = 1+nτ[k-1]:nτ[k]
        append!(tₗ, tₗₖ[k] .+ p[:δ]*δtₗ[arrI]*tₗₖ[k]/p[:t])
        append!(tᵣ, tᵣₖ[k] .+ p[:δ]*δtᵣ[arrI]*tᵣₖ[k]/p[:t])
        append!(ε , p[:ε]  .+ p[:δ]*δε[arrI])
        append!(εₘ, p[:εₘ] .+ p[:δ]*δεₘ[arrI])
    end
    return tₗ,tᵣ,ε,εₘ,τ,(nτ[τI].+1)
end

function tqdpulseC(i::Int,τₖ::Real;p...)
    ∠  = ∠HZH[i,:]
    tₗ = zeros(0)
    tᵣ = zeros(0)
    τ  = zeros(0)
    if i == 11
        tₗₖ,tᵣₖ,τₖ = tqdpulseH(τₖ;p...)
        push!(tₗ,tₗₖ)
        push!(tᵣ,tᵣₖ)
        push!(τ,τₖ)
    elseif i == 12
        tₗₖ,tᵣₖ,τₖ = tqdpulseabstract([-1,0,1],1,τₖ;p...)
        push!(tₗ,tₗₖ)
        push!(tᵣ,tᵣₖ)
        push!(τ,τₖ)
    else
        for k in 1:3
            ∠[k] == nothing && continue
            if k == 2
                tₗₖ,tᵣₖ,τₖ = tqdpulseH(τₖ;p...)
                push!(tₗ,tₗₖ)
                push!(tᵣ,tᵣₖ)
                push!(τ,τₖ)
            end
            tₗₖ,tᵣₖ,τₖ = tqdpulseZ(∠[k],τₖ;p...)
            push!(tₗ,tₗₖ)
            push!(tᵣ,tᵣₖ)
            push!(τ,τₖ)
            if k == 2
                tₗₖ,tᵣₖ,τₖ = tqdpulseH(τₖ;p...)
                push!(tₗ,tₗₖ)
                push!(tᵣ,tᵣₖ)
                push!(τ,τₖ)
            end
        end
    end
    return tₗ,tᵣ,τ
end
tqdpulseH(τ::Real;p...) = tqdpulseabstract([1,0,1],1,τ;p...)
tqdpulseZ(θ::Real,τ::Real;p...) = tqdpulseabstract([0,0,1],θ,τ;p...)

function tqdpulseabstract(n::AbstractArray{<:Real},θ::Real,τ::Real;p...)
    n = normalize(vec(n))
    θ = (2ceil(θ/2)-θ)%2
    θ ≈ 0 && (θ = 2)
    g₊ = 1/(p[:U]-2p[:V]-p[:εₘ]+p[:ε]) + 1/(p[:U]+p[:εₘ]-p[:ε])
    g₋ = 1/(p[:U]-2p[:V]-p[:εₘ]-p[:ε]) + 1/(p[:U]+p[:εₘ]+p[:ε])

    N = 4p[:t]^2/abs(p[:U]-p[:V])*√(n[1]^2 + 1)
    τ += ħ/2N * θ*π
    tₗ = √( N/2g₊ * (n[3] + n[1]/√3) )
    tᵣ = √( N/2g₋ * (n[3] - n[1]/√3) )
    return tₗ,tᵣ,τ
end

function tqdH(tₗ::Real,tᵣ::Real,ε::Real,εₘ::Real,τ::Real;p...)
    g₊ = 1 ./ (p[:U]-2p[:V].-εₘ+ε) + 1 ./ (p[:U].+εₘ-ε)
    g₋ = 1 ./ (p[:U]-2p[:V].-εₘ-ε) + 1 ./ (p[:U].+εₘ+ε)
    n₁ = √3( tₗ.^2 .* g₊ - tᵣ.^2 .* g₋ )
    n₃ =   ( tₗ.^2 .* g₊ + tᵣ.^2 .* g₋ )
    N  = √(n₁^2 + n₃^2)
    if N ≈ 0#isnan(n₁/N) || isnan(n₃/N)
        return I
    else
        return cos(τ*N/ħ)I + im*sin(τ*N/ħ)*(n₁/N*[0 1; 1 0] + n₃/N*[1 0; 0 -1])
    end
end

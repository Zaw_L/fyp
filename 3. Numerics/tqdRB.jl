include("tqd.jl")
include("../lib/cliffordGates.jl")
include("../lib/eigensolve.jl")

function tqdRB(M,K;ρ=[1,0]*[1,0]',q=copy(ρ),verbose=0,params...)
    #M = M₀:ΔM:M
    F  = zeros(length(M), K)
    Q  = zeros(length(M), K)
    T  = zeros(length(M), K)
    F̄  = 0

    𝓖𝒢 = zeros(16,16)
    𝓖 = zeros(4,4)
    𝒢 = zeros(4,4)
    for m in 1:length(M)
        for r in 1:K
            S  = rand(1:24,M[m])
            Sᵢ = reduce(getCproduct, reverse(S))
            push!(S, getCinverse(Sᵢ))

            tₗ,tᵣ,ε,εₘ,τ,τI = tqdnoiseypulseseq(S;params...)

            G = I
            for k in 1:M[m]+1

                C̃ = I

                for j in τI[k]+1:τI[k+1]
                    R = tqdH(tₗ[j],tᵣ[j],ε[j],εₘ[j],τ[j]-τ[j-1];params...)
                    C̃ = R*C̃
                end

                C = rotateC(S[k])

                G = C̃*G

                𝓒 = vecU(C)
                𝒞 = vecU(C̃)

                𝓖  += 𝓒
                𝒢  += 𝒞
                𝓖𝒢 += [zeros(4) 𝓒[:,2:end]] ⊗ 𝒞
                F̄  += fidelity(C̃,C)
            end
            F[m,r] = tr(q*G*ρ*G')
            T[m,r] = τ[end]
        end
    end
    𝓖  /= sum((M.+1)*K)
    𝒢  /= sum((M.+1)*K)
    𝓖𝒢 /= sum((M.+1)*K)
    F̄  /= sum((M.+1)*K)
    p̄  = collect(analyticalRB(𝓖,𝒢,𝓖𝒢,ρ,q,returnAll=false))
    return F,T,Q,p̄,F̄
end

fidelity(C̃,C) = 1/3 * (
        abs2(dot([1,0], C̃*C' * [1,0])) +
        abs2(dot([0,1], C̃*C' * [0,1])) +
        real(
            dot([1,0], C̃*C' * [1,0]) *
            dot([0,1], C*C̃' * [0,1])
        )
) + 1/6 * (
    abs2(dot([0,1], C̃*C' * [1,0])) +
    abs2(dot([1,0], C̃*C' * [0,1]))
)

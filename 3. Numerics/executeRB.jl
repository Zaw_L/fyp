using LsqFit, JSON, Dates, DataStructures
import Statistics.mean, Statistics.std

include("tqdRB.jl")
include("../lib/tqdParams.jl")

hasMPI = try
            using MPI
            true
         catch err
            false
         end

if hasMPI
    MPI.Init()
    comm = MPI.COMM_WORLD
    commSize = MPI.Comm_size(comm)
    rank = MPI.Comm_rank(comm)
else
    commSize = 1
    rank = 0
end

if rank == 0
    t1 = now()
    _params = try
            JSON.parsefile(ARGS[1],dicttype=DataStructures.OrderedDict)
        catch err
            @warn err
            false
        end
    if _params != false
        dir = try pop!(_params, "dir") catch err "" end
        _params["M"] = stepstoarray(_params["M"])
        loopKey, loopVal, noloopParams = initparams(_params)
        vals = collect(Iterators.product(loopVal...))
        states = Array{Array}(undef,commSize)
        each = length(vals) ÷ commSize
        for r in 1:commSize
            eachTo = r*each
            if r == commSize
                eachTo += length(vals)%commSize
            end
            states[r] = ((r-1)*each+1):eachTo
        end
    end
else
    dir = Nothing
    _params = Nothing
    loopKey, loopVal, noloopParams = Nothing, Nothing, Nothing
    vals = Nothing
    states = Nothing
end

hasMPI && (_params = MPI.bcast(_params,0,comm))
_params == false && quit()


if hasMPI
    dir = MPI.bcast(dir,0,comm)
    loopKey = MPI.bcast(loopKey,0,comm)
    loopVal = MPI.bcast(loopVal,0,comm)
    noloopParams = MPI.bcast(noloopParams,0,comm)
    vals = MPI.bcast(vals,0,comm)
    states = MPI.bcast(states,0,comm)
end

p₀ = [1-1E-9,.5,.5]
pₗ = [0.,0.,0.]
pᵤ = [1.,1.,1.]
modelF(m,p) = p[2]*p[1].^m .+ p[3]
jacobF(m,p) = [ p[2] .* m .* p[1].^(m.-1) p[1].^m  ones(length(m)) ]

modelQ(m,p) = p[2]*p[1].^(m.-1) .+ p[3]
jacobQ(m,p) = [ p[2] .* (m.-1) .* p[1].^(m.-2) p[1].^(m.-1)  ones(length(m)) ]

stateFilename = paramfilename(_params, dir, ".skipStates.json")
skipStates = try
                 JSON.parsefile(stateFilename)
             catch err
                 []
             end

for state in states[rank+1]
    global skipStates
    if state in skipStates
        continue
    end
    val = vals[state]
    _params = copy(noloopParams)
    alert = ""
    for i in 1:length(loopKey)
        push!(_params, loopKey[i]=>val[i])
        if alert != ""
            alert *= ", "
        end
        alert *= "$(loopKey[i]) = $(val[i])"
    end
    println("Rank $rank started $alert on $(now())")
    t0 = now()
    F,T,Q,p̄,F̄ = tqdRB(
        _params["M"],
        _params["rep"];
        [Symbol(k) => v for (k,v) in _params]...
    )
    println("Rank $rank took $(now() - t0)")
    μF = vec(mean(F,dims=2))
    σF = vec(std(F,dims=2))/√(_params["rep"]+1)
    μQ = vec(mean(Q,dims=2))
    σQ = vec(std(Q,dims=2)/√(_params["rep"]+1))

    p = copy(p₀)
    σp = fill(Inf,length(p))
    u = copy(p₀)
    σu = fill(Inf,length(u))
    try
        fitF = curve_fit(modelF, jacobF, _params["M"], μF, σF.^(-2), p₀, lower=pₗ, upper=pᵤ)
        fitF.converged && @warn "Fit did not converge"
        p = coef(fitF)
        σp = margin_error(fitF)
    catch err
        println(length(_params["M"]), length(μF))
        @warn err
    end
    try
        fitQ = curve_fit(modelQ, jacobQ, _params["M"], μQ, σQ.^(-2), p₀, lower=pₗ, upper=pᵤ)
        fitQ.converged && @warn "Fit did not converge"
        u = coef(fitQ)
        σu = margin_error(fitQ)
    catch err
        @warn err
    end

    open(paramfilename(_params, dir, ".json"),"w") do f
        JSON.print(f, DataStructures.OrderedDict([
            "iF" => 1 .- F,
            "iμF" => 1 .- μF,
            "σF" => σF,
            "T" => T,
            "μQ" => μQ,
            "σQ" => σQ,
            "p" => p,
            "σp" => σp,
            "p̄" => p̄,
            "u" => u,
            "r̄" => 1 - F̄
        ]))
    end
    _skipStates = skipStates
    skipStates = try
                     JSON.parsefile(stateFilename)
                 catch err
                      @warn err
                     _skipStates
                 end
    push!(skipStates,state)
    open(stateFilename,"w") do f
        JSON.print(f, skipStates)
    end
    println("Parameters $(length(skipStates)) of $(length(vals)) finished")
end

if rank == 0
    if hasMPI
        for r in 2:commSize-1
            println(MPI.recv(r,0,comm)[1])
        end
    end
    open(stateFilename,"w") do f
        JSON.print(f, [])
    end
    println("RB experiment $(ARGS[1]) is finished, and it took $(now() - t1).")
else
    MPI.send("Rank $rank is finished",0,0,comm)
end

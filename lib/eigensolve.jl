using LinearAlgebra

⊗(A::AbstractArray, B::AbstractArray) = kron(A, B)

function vecA(A)
    return [tr(σₙ[i]'A) for i in 1:4]
end

function vecC(𝓒)
    return [tr(σₙ[i]'𝓒(σₙ[j])) for i in 1:4, j in 1:4]
end

function vecU(U)
    return vecC(A->U*A*U')
end

function analyticalRB(𝓖,𝒢,𝓖𝒢,ρ=vecA([1 0; 0 0]),𝓜=copy(ρ);returnAll=true)
    #𝓖𝒢 = [ [zeros(4) 𝓖[k][:,2:end]] ⊗ 𝒢[k] for k in 1:length(𝓖) ]
    A = []
    B = []
    p, v𝓛ᵤₖ = eigen(𝓖𝒢)
    t, L₀ₖ = eigen(𝒢)
    reverse!(p)
    reverse!(t)
    v𝓡ᵤₖ = transpose(inv(v𝓛ᵤₖ))
    R₀ₖ = transpose(inv(L₀ₖ))
    for i in 0:length(t)-1
        v𝓛ᵤ = v𝓛ᵤₖ[:,end-i]
        v𝓡ᵤ = v𝓡ᵤₖ[:,end-i]
        𝓛ᵤ = √3*reshape(v𝓛ᵤ,4,4)
        𝓡ᵤ = √3*reshape(transpose(v𝓡ᵤ),4,4)

        L₀ = L₀ₖ[:,end-i]
        R₀ = R₀ₖ[:,end-i]

        push!(A, dot(vecA(𝓜), 𝓛ᵤ * 𝓡ᵤ * vecA(ρ)))
        #push!(A, dot(vecA(𝓜), 𝓛ᵤ * vecA(ρ)))
        push!(B, dot(vecA(𝓜), L₀) * dot(R₀, vecA(ρ)))
    end

    if !returnAll
        A = A[1]
        p = p[1]
        B = B[1]
        t = t[1]
    end
    return real.((A,p,B,t))
end

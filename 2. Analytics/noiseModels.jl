using LinearAlgebra
include("../lib/cliffordGates.jl")

function rotate3D(m,ϑ)
    m = normalize(m)
    mₓ = zeros(3,3)
    for i in 1:3
        mₓ[:,i] = m × (1:3 .== i)
    end
    return cos(ϑ*π)I + sin(ϑ*π)mₓ + (1-cos(ϑ*π))m*m'
end

function boundinfidelity(r)
    if r < 0 || r > 2/3
        throw(DomainError(r, "The gate infidelity must be bounded by 0 ≤ r ≤ ⅔"))
    end
end

function randomΛ₁(r,Nₖ=5;maxLoop=1000)
    L = 2rand(3,3) .- 1
    L *= 3*(1-2r)/tr(L)
    maxL = maximum(abs.(eigvals(L)))
    if maxL > 1
        L /= maxL
    end
    return rand()*r*normalize(2rand(3) .- 1), L
end

function randomΛ(r,Nₖ=5;maxLoop=1000)
    if r < 0 || r >= 1/2
        throw(DomainError(r, "The gate infidelity for random errors must be bounded by 0 ≤ r < ½"))
    end
    δ = 10atanh(2*r)/7
    r̄ = 0
    λ,Λ = zeros(3),I
    while r̄ < r && maxLoop > 0
        Lₖ  = [I + δ*((2rand(2,2) .- 1) + im*(2rand(2,2) .- 1)) for k in 1:Nₖ-1]
        B   = sum(Lₖ[k]'Lₖ[k] for k in 1:Nₖ-1)
        Lₖ = Lₖ./√eigmax(B)*(1-1e-9)
        B = I - sum(Lₖ[k]'Lₖ[k] for k in 1:Nₖ-1)
        #C   = √eigmax(B)*(1-1e-9)
        #Lₖ  = Lₖ./C
        #B   = I - sum(Lₖ[k]'Lₖ[k] for k in 1:Nₖ-1)#B/C^2
        U = rotatenθ(normalize(2rand(3).-1),2δ*π)
        push!(Lₖ,U*cholesky(B).U)
        Λ = real.([
            sum(tr(σ(i)Lₖ[k]σ(j)Lₖ[k]') for k in 1:Nₖ)
            for i in 0:3,j in 0:3
        ])
        λ,Λ = Λ[2:end,1],Λ[2:end,2:end]
        r̄ = infidelity(I,(λ,Λ))
        maxLoop -= 1
    end
    if r̄ > r
        λ,Λ = r/r̄*λ, r/r̄*Λ + (1-r/r̄)*I
    end
    return λ,Λ
end

function addnoise(𝔾,r₀,dep=0,r=r₀;genΛ=randomΛ,genΛₖ=false)
    r₀ = extrema(r₀)
    r₀ = (r₀[2]-r₀[1])*rand() + r₀[1]
    r = extrema(r)

    if genΛₖ == false
        genΛₖ = genΛ
    end

    𝓵,𝓛 = zeros(3),I
    if genΛ isa Tuple
        pₗ = rand()
        𝓵,𝓛 = genΛ[1](pₗ*r₀)
        𝓻,𝓡 = genΛ[2]((1-pₗ)*r₀)
    else
        𝓻,𝓡 = genΛ(r₀)
    end

    𝕘 = [(zeros(3),g) for g ∈ 𝔾]
    for k ∈ 1:length(𝕘)
        if dep == 0
            𝓡ₖ = 𝓡
            𝓛ₖ = 𝓛
            𝓻ₖ = 𝓻
            𝓵ₖ = 𝓵
        else
            rₖ = (r[2]-r[1])*rand() + r[1]
            𝓵ₖ,𝓛ₖ = zeros(3),I
            if genΛₖ isa Tuple
                δₗ = rand()
                𝓵ₖ,𝓛ₖ = genΛₖ[1](δₗ*rₖ)
                𝓻ₖ,𝓡ₖ = genΛₖ[2]((1-δₗ)*rₖ)
            else
                𝓻ₖ,𝓡ₖ = genΛₖ(rₖ)
            end
            𝓡ₖ = (1-dep)*𝓡 + dep*𝓡ₖ
            𝓛ₖ = (1-dep)*𝓛 + dep*𝓛ₖ
            𝓵ₖ = (1-dep)*𝓵 + dep*𝓵ₖ
            𝓻ₖ = (1-dep)*𝓻 + dep*𝓻ₖ
        end
        𝕘[k] = 𝓵ₖ+𝓻ₖ, 𝓛ₖ*𝕘[k][2]*𝓡ₖ
    end
    return 𝕘
end

function infidelity(G,G̃)
    if G̃ isa Tuple
        G̃ = G̃[2]
    end
    A = G'G̃
    if A isa UniformScaling
        A *= I(3)
    end
    return 1/2-real(tr(A))/6
end
